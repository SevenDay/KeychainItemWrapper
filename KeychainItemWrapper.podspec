
Pod::Spec.new do |s|
  s.name         = "KeychainItemWrapper"
  s.version      = "1.2.0"
  s.summary      = "KeychainItemWrapper lib."
  s.description  = <<-DESC  "KeychainItemWrapper for sevendays"
                   DESC
  s.homepage     = "https://gitlab.com/SevenDay/KeychainItemWrapper"

  s.license      = ""
  s.author             = { "zhang.wenhai" => "zhang_mr1989@163.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "git@gitlab.com:SevenDay/KeychainItemWrapper.git" }

  s.source_files  = "Classes/**/*.{h,m}"
  #s.exclude_files = "Classes/Exclude"

  s.framework  = "Security"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
